# git-dev

This program serves files on the filesystem as git repositories over http.

Only version 1 of the git http protocol is supported.

## Building

The binary package for nix can be built as follows:

```sh
nix build
```

The resulting binary will be found in `result/bin/git-dev`.

A docker image can be built as follows:

```sh
docker load <"$(nix build .#image --print-out-paths --no-link)"
```

This loads an image tagged `registry.gitlab.com/slagit/git-dev:latest`.

## Running

The server listens on port 8000 and can be launched as follows:

```sh
git-dev reponame.git=/path/to/repo ...
```

For example, to serve the current directory as http://localhost:8000/demo.git:

```sh
git-dev demo.git=$PWD
```

## Ignoring files

To avoid conflicts, the server will ignore any file or directory named
`.git`. It will also honor any `.gitignore` files that it finds.

To allow the served content to be further limited, the server will honor
`.gitdevignore` files with the same semantics as `gitignore`. Note that
`.gitignore` files are applied separately from `.gitdevignore` files so,
for example, you can't exclude a file in `.gitdevignore` that was ignored
by `.gitignore`.
