package main

import (
	"bufio"
	"errors"
	"io"
	iofs "io/fs"
	"path"
	"regexp"
	"strings"
)

type Ignore struct {
	isDir   bool
	include bool
	regexp  *regexp.Regexp
}

func trimUnescaped(l string) string {
	var r []string
	for i := 0; i < len(l); i++ {
		if i+1 < len(l) && l[i] == '\\' {
			r = append(r, l[i:i+2])
			i++
		} else {
			r = append(r, l[i:i+1])
		}

	}
	for r[len(r)-1] == " " {
		r = r[:len(r)-1]
	}
	return strings.Join(r, "")
}

func chr2re(ch byte) string {
	if ch == '.' {
		return "\\."
	}
	if ch == ')' {
		return "\\)"
	}
	if ch == '(' {
		return "\\("
	}
	if ch == '$' {
		return "\\$"
	}
	if ch == '^' {
		return "\\^"
	}
	if ch == '[' {
		return "\\["
	}
	if ch == ']' {
		return "\\]"
	}
	return string(ch)
}

func ParseLine(pf, l string) Ignore {
	var i Ignore
	if l[0] == '!' {
		i.include = true
		l = l[1:]
	}
	l = trimUnescaped(l)
	if l[len(l)-1] == '/' {
		i.isDir = true
		l = l[:len(l)-1]
	}
	if !strings.Contains(l, "/") {
		l = "**/" + l
	}
	// TODO: remove initial/consecutive slashes
	var re strings.Builder
	re.WriteString("^")
	for x := range pf {
		re.WriteString(chr2re(pf[x]))
	}
	if len(l) >= 3 && l[:3] == "**/" {
		re.WriteString("([^/]*/)*")
		l = l[3:]
	}
	for len(l) > 0 {
		if len(l) > 2 && l[0] == '\\' {
			re.WriteString(chr2re(l[1]))
			l = l[2:]
		} else if len(l) >= 4 && l[:4] == "/**/" {
			re.WriteString("/([^/]*/)*")
			l = l[4:]
		} else if len(l) == 2 && l == "**" {
			re.WriteString(".*")
			l = l[2:]
		} else if l[0] == '*' {
			re.WriteString("[^/]*")
			l = l[1:]
		} else if l[0] == '?' {
			re.WriteString("[^/]")
			l = l[1:]
		} else {
			re.WriteString(chr2re(l[0]))
			l = l[1:]
		}
	}
	re.WriteString("$")
	i.regexp = regexp.MustCompile(re.String())
	return i
}

func (i *Ignore) Match(path string, isDir bool) bool {
	if !isDir && i.isDir {
		return false
	}
	return i.regexp.Match([]byte(path))
}

type IgnoreList struct {
	list   []Ignore
	parent *IgnoreList
}

func Read(p *IgnoreList, pf string, r io.Reader) (*IgnoreList, error) {
	il := IgnoreList{
		parent: p,
	}
	s := bufio.NewScanner(r)
	for s.Scan() {
		l := s.Text()
		if l[0] != '#' && len(strings.TrimSpace(l)) > 0 {
			il.list = append(il.list, ParseLine(pf, l))
		}
	}
	if err := s.Err(); err != nil {
		return nil, err
	}
	return &il, nil
}

func LoadFromFile(fs iofs.FS, p *IgnoreList, fn string) (*IgnoreList, error) {
	pf := path.Dir(fn)
	if pf == "." {
		pf = ""
	} else {
		pf += "/"
	}

	r, err := fs.Open(fn)
	if errors.Is(err, iofs.ErrNotExist) {
		return &IgnoreList{
			parent: p,
		}, nil
	}
	if err != nil {
		return nil, err
	}
	defer r.Close()

	return Read(p, pf, r)
}

func (il *IgnoreList) Include(path string, isDir bool) bool {
	if il == nil {
		return true
	}
	for i := len(il.list) - 1; i >= 0; i-- {
		if il.list[i].Match(path, isDir) {
			return il.list[i].include
		}
	}
	return il.parent.Include(path, isDir)
}
