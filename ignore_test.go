package main

import (
	"testing"
)

type ignoreTest struct {
	prefix    string
	line      string
	matches   []string
	unmatches []string
	isDir     bool
	include   bool
}

func (r ignoreTest) Run(t *testing.T) {
	i := ParseLine(r.prefix, r.line)
	for _, m := range r.matches {
		if !i.regexp.Match([]byte(m)) {
			t.Errorf("%s does not match %s", r.line, m)
		}
	}
	for _, m := range r.unmatches {
		if i.regexp.Match([]byte(m)) {
			t.Errorf("%s matches %s", r.line, m)
		}
	}
	if r.isDir != i.isDir {
		t.Errorf("isDir does not match (expected: %t, actual: %t)", r.isDir, i.isDir)
	}
	if r.include != i.include {
		t.Errorf("include does not match (expected: %t, actual: %t)", r.include, i.include)
	}
}

func TestIgnoreExtention(t *testing.T) {
	ignoreTest{
		line: "*.txt",
		matches: []string{
			"a/b/c.txt",
			"c.txt",
		},
		unmatches: []string{
			"cxtxt",
			"c.md",
		},
	}.Run(t)
}

func TestIgnoreDoubleStart(t *testing.T) {
	ignoreTest{
		line: "**/foo",
		matches: []string{
			"foo",
			"a/foo",
			"a/b/foo",
		},
		unmatches: []string{
			"a/b",
		},
	}.Run(t)
}

func TestIgnoreExactPath(t *testing.T) {
	ignoreTest{
		line: "a/b/c",
		matches: []string{
			"a/b/c",
		},
		unmatches: []string{
			"a",
			"b",
			"c",
			"x/a/b/c",
			"a/b/c/d",
			"abc",
		},
	}.Run(t)
}

func TestIgnoreDoubleMiddle(t *testing.T) {
	ignoreTest{
		line: "foo/**/bar",
		matches: []string{
			"foo/bar",
			"foo/x/bar",
			"foo/x/y/bar",
		},
		unmatches: []string{
			"foobar",
		},
	}.Run(t)
}

func TestIgnoreAllFiles(t *testing.T) {
	ignoreTest{
		line: "**",
		matches: []string{
			"foo/bar",
			"foo/x/bar",
			"foo/bar",
		},
	}.Run(t)
}

func TestIgnoreAllDirectories(t *testing.T) {
	ignoreTest{
		line: "**/",
		matches: []string{
			"foo/bar",
			"foo/x/bar",
			"foo/bar",
		},
		isDir: true,
	}.Run(t)
}

func TestIgnoreInclude(t *testing.T) {
	ignoreTest{
		line:    "!*.txt",
		include: true,
	}.Run(t)
}

func TestIgnorePrefix(t *testing.T) {
	ignoreTest{
		prefix: "a/",
		line:   "*.txt",
		matches: []string{
			"a/b.txt",
			"a/b/c.txt",
		},
		unmatches: []string{
			"a.txt",
			"b/a.txt",
		},
	}.Run(t)
}
