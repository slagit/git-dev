{
  description = "Serve local files as a git repository";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    gomod2nix = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:nix-community/gomod2nix";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/master";
  };
  outputs = { self, flake-utils, gomod2nix, nixpkgs }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            gomod2nix.overlays.default
          ];
        };
      in rec {
        apps = {
          dev-gofmt = let
            script = pkgs.writeShellScriptBin "git-dev-dev-gofmt" ''
              set -euo pipefail
              ${pkgs.go}/bin/gofmt -w -s .
            '';
          in {
            type = "app";
            program = "${script}/bin/git-dev-dev-gofmt";
          };
          dev-mod-update = let
            script = pkgs.writeShellScriptBin "git-dev-dev-mod-update" ''
              set -euo pipefail
              ${pkgs.go}/bin/go mod tidy
              ${pkgs.gomod2nix}/bin/gomod2nix
            '';
          in {
            type = "app";
            program = "${script}/bin/git-dev-dev-mod-update";
          };
        };
        checks = {
          gofmt = pkgs.runCommand "git-dev-gofmt" {} ''
            set -euo pipefail
            test "$(${pkgs.go}/bin/gofmt -s -l ${pkgs.lib.cleanSource ./.} | wc -l)" -eq 0
            touch $out
          '';
          statix = pkgs.runCommand "statix-test" {} ''
            ${pkgs.statix}/bin/statix check ${./.}
            touch $out
          '';
        };
        packages = {
          default = packages.git-dev;
          git-dev = pkgs.buildGoApplication {
            modules = ./gomod2nix.toml;
            name = "git-dev";
            pwd = ./.;
            src = pkgs.lib.cleanSource ./.;
          };
          image = pkgs.dockerTools.buildImage {
            name = "registry.gitlab.com/slagit/git-dev";
            tag = "latest";
            copyToRoot = packages.git-dev;
            config = {
              Entrypoint = ["/bin/git-dev"];
              ExposedPorts = {
                "8000/tcp" = {};
              };
            };
          };
        };
      }
    );
}
