package main

import (
	"bytes"
	"compress/zlib"
	"crypto/sha1"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"hash"
	"io"
	iofs "io/fs"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
)

type GitObject interface {
	Content() []byte
	PackType() int
	Type() string
}

func GetDigest(o GitObject) []byte {
	c := o.Content()
	h := sha1.New()
	fmt.Fprintf(h, "%s %d\x00", o.Type(), len(c))
	h.Write(c)
	return h.Sum(nil)
}

type HashedWriter struct {
	h hash.Hash
	w io.Writer
}

func (hw *HashedWriter) Sum(b []byte) []byte {
	return hw.h.Sum(b)
}

func (hw *HashedWriter) Write(p []byte) (int, error) {
	hw.h.Write(p)
	return hw.w.Write(p)
}

func WritePak(w io.Writer, c *Commit) error {
	hw := HashedWriter{
		h: sha1.New(),
		w: w,
	}

	used := make(map[string]bool)
	for _, o := range c.Tree.Blobs {
		used[hex.EncodeToString(GetDigest(o))] = true
	}
	used[hex.EncodeToString(GetDigest(c.Tree))] = true
	used[hex.EncodeToString(GetDigest(c))] = true

	if _, err := hw.Write([]byte("PACK")); err != nil {
		return err
	}
	if err := binary.Write(&hw, binary.BigEndian, uint32(2)); err != nil {
		return err
	}
	if err := binary.Write(&hw, binary.BigEndian, uint32(len(used))); err != nil {
		return err
	}

	written := make(map[string]bool)
	writeObj := func(o GitObject) error {
		d := hex.EncodeToString(GetDigest(o))
		if !written[d] {
			c := o.Content()
			if _, err := hw.Write(typeSize(o.PackType(), len(c))); err != nil {
				return err
			}
			zw := zlib.NewWriter(&hw)
			if _, err := zw.Write(c); err != nil {
				return err
			}
			if err := zw.Close(); err != nil {
				return err
			}
			written[d] = true
		}
		return nil
	}

	for _, o := range c.Tree.Blobs {
		if err := writeObj(o); err != nil {
			return err
		}
	}

	if err := writeObj(c.Tree); err != nil {
		return err
	}

	if err := writeObj(c); err != nil {
		return err
	}

	d := hw.Sum(nil)
	if _, err := w.Write(d); err != nil {
		return nil
	}

	return nil
}

type Blob struct {
	content []byte
	mode    string
	path    string
	time    time.Time
}

func NewFileBlob(fs LstatFS, path string) (*Blob, error) {
	fi, err := fs.Lstat(path)
	if err != nil {
		return nil, err
	}
	var mode string
	if fi.Mode()&0100 == 0 {
		mode = "100644"
	} else {
		mode = "100755"
	}
	content, err := iofs.ReadFile(fs, path)
	if err != nil {
		return nil, err
	}
	return &Blob{
		content: content,
		mode:    mode,
		path:    path,
		time:    fi.ModTime(),
	}, nil
}

func NewSymlinkBlob(fs ReadlinkFS, path string) (*Blob, error) {
	target, err := fs.Readlink(path)
	if err != nil {
		return nil, err
	}
	return &Blob{
		content: []byte(target),
		mode:    "120000",
		path:    path,
	}, nil
}

func (b *Blob) Content() []byte {
	return b.content
}

func (*Blob) PackType() int {
	return 3
}

func (*Blob) Type() string {
	return "blob"
}

type Tree struct {
	Blobs []*Blob
}

func (t *Tree) Content() []byte {
	var buf bytes.Buffer
	for _, b := range t.Blobs {
		d := GetDigest(b)
		fmt.Fprintf(&buf, "%s %s\x00", b.mode, b.path)
		buf.Write(d)
	}
	return buf.Bytes()
}

func (*Tree) PackType() int {
	return 2
}

func (*Tree) Type() string {
	return "tree"
}

type Author struct {
	Name  string
	Email string
}

type Commit struct {
	Author    Author
	Committer Author
	Message   string
	Tree      *Tree
}

func readDir(fs iofs.FS, name string) ([]iofs.DirEntry, error) {
	f, err := fs.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	fd, ok := f.(iofs.ReadDirFile)
	if !ok {
		return nil, fmt.Errorf("Not an fs.ReadDirFile: %s", name)
	}

	dirs, err := fd.ReadDir(-1)
	sort.Slice(dirs, func(i, j int) bool {
		return dirs[i].Name() < dirs[j].Name()
	})
	return dirs, err
}

type loadDirParams struct {
	gitIgnore    *IgnoreList
	gitDevIgnore *IgnoreList
}

func loadDir(fs LstatReadlinkFS, root string, commit *Commit, ldp loadDirParams) error {
	gitIgnore, err := LoadFromFile(fs, ldp.gitIgnore, filepath.Join(root, ".gitignore"))
	if err != nil {
		return err
	}
	gitDevIgnore, err := LoadFromFile(fs, ldp.gitDevIgnore, filepath.Join(root, ".gitdevignore"))
	if err != nil {
		return err
	}
	ds, err := readDir(fs, root)
	if err != nil {
		return err
	}
	for _, d := range ds {
		if d.Name() == ".git" {
			continue
		}
		path := filepath.Join(root, d.Name())
		if !gitIgnore.Include(path, d.IsDir()) {
			continue
		}
		if !gitDevIgnore.Include(path, d.IsDir()) {
			continue
		}
		switch d.Type() & iofs.ModeType {
		case 0:
			b, err := NewFileBlob(fs, path)
			if err != nil {
				return err
			}
			commit.Tree.Blobs = append(commit.Tree.Blobs, b)
		case iofs.ModeSymlink:
			b, err := NewSymlinkBlob(fs, path)
			if err != nil {
				return err
			}
			commit.Tree.Blobs = append(commit.Tree.Blobs, b)
		case iofs.ModeDir:
			if err := loadDir(fs, path, commit, loadDirParams{
				gitIgnore:    gitIgnore,
				gitDevIgnore: gitDevIgnore,
			}); err != nil {
				return err
			}
		}
	}
	return nil
}

func Load(fs LstatReadlinkFS) (*Commit, error) {
	uid := os.Getuid()
	username := strconv.Itoa(uid)
	name := "Unknown User"
	if u, err := user.LookupId(username); err == nil {
		username = u.Username
		name = u.Name
	}
	h, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	commit := &Commit{
		Author: Author{
			Name:  name,
			Email: fmt.Sprintf("%s@%s", username, h),
		},
		Committer: Author{
			Name:  name,
			Email: fmt.Sprintf("%s@%s", username, h),
		},
		Message: fmt.Sprintf("Generated by `git-dev`\n\nThis commit was automatically generated by `git-dev` from the contents of `%s`.\n", cwd),
		Tree:    &Tree{},
	}
	if err := loadDir(fs, ".", commit, loadDirParams{}); err != nil {
		return nil, err
	}
	return commit, nil
}

func (c *Commit) Content() []byte {
	var lasttime time.Time
	for _, b := range c.Tree.Blobs {
		if b.time.After(lasttime) {
			lasttime = b.time
		}
	}
	_, off := lasttime.Zone()
	off /= 60
	sign := "+"
	if off < 0 {
		sign = "-"
		off = -off
	}
	stamp := fmt.Sprintf(
		"%d %s%02d%02d",
		lasttime.Unix(),
		sign,
		off/60,
		off%60,
	)
	d := GetDigest(c.Tree)
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "tree %s\n", hex.EncodeToString(d))
	fmt.Fprintf(&buf, "author %s <%s> %s\n", c.Author.Name, c.Author.Email, stamp)
	fmt.Fprintf(&buf, "committer %s <%s> %s\n", c.Committer.Name, c.Committer.Email, stamp)
	fmt.Fprintln(&buf, "")
	fmt.Fprintln(&buf, c.Message)
	return buf.Bytes()
}

func (*Commit) PackType() int {
	return 1
}

func (*Commit) Type() string {
	return "commit"
}

func setNoCache(w http.ResponseWriter) {
	w.Header().Set("Cache-Control", "no-cache, max-age=0, must-revalidate")
	w.Header().Set("Expires", "Fri, 01 Jan 1980 00:00:00 GMT")
	w.Header().Set("Pragma", "no-cache")
}

var Flush = errors.New("FLUSH")

type PacketReader struct {
	Reader io.Reader
}

func (pr *PacketReader) ReadLine() (string, error) {
	pkt, err := pr.ReadPacket()
	if err != nil {
		return "", err
	}
	strpkt := string(pkt)
	if strpkt[len(strpkt)-1] == '\n' {
		strpkt = strpkt[:len(strpkt)-1]
	}
	return strpkt, nil
}

func (pr *PacketReader) ReadPacket() ([]byte, error) {
	sizebuf := make([]byte, 4)
	if _, err := io.ReadFull(pr.Reader, sizebuf); err != nil {
		return nil, err
	}
	size, err := strconv.ParseUint(string(sizebuf), 16, 64)
	if err != nil {
		return nil, err
	}
	if size == 0 {
		return nil, Flush
	}
	pkt := make([]byte, size-4)
	if _, err := io.ReadFull(pr.Reader, pkt); err == io.EOF {
		return nil, io.ErrUnexpectedEOF
	} else if err != nil {
		return nil, err
	}
	return pkt, nil
}

type PacketWriter struct {
	Writer io.Writer
}

func (pw *PacketWriter) Flush() error {
	_, err := pw.Writer.Write([]byte("0000"))
	return err
}

func (pw *PacketWriter) Write(p []byte) (int, error) {
	if _, err := fmt.Fprintf(pw.Writer, "%04x", len(p)+4); err != nil {
		return 0, err
	}
	return pw.Writer.Write(p)
}

type GitUploadPackHandler struct {
	FS LstatReadlinkFS
}

func (h GitUploadPackHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c, err := Load(h.FS)
	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type", "application/x-git-upload-pack-result")
	pw := &PacketWriter{
		Writer: w,
	}
	fmt.Fprintln(pw, "NAK")
	if err := WritePak(w, c); err != nil {
		panic(err)
	}
}

type InfoRefsHandler struct {
	FS LstatReadlinkFS
}

func (h InfoRefsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	service := r.URL.Query()["service"]
	if len(service) > 0 && service[0] == "git-upload-pack" {
		c, err := Load(h.FS)
		if err != nil {
			panic(err)
		}
		d := hex.EncodeToString(GetDigest(c))

		w.Header().Set("Content-Type", "application/x-git-upload-pack-advertisement")
		pw := &PacketWriter{
			Writer: w,
		}
		fmt.Fprintln(pw, "# service=git-upload-pack")
		pw.Flush()
		fmt.Fprintf(pw, "%s HEAD\x00symref=HEAD:refs/heads/main\n", d)
		fmt.Fprintf(pw, "%s refs/heads/main\n", d)
		pw.Flush()
		return
	}
	w.WriteHeader(http.StatusForbidden)
}

func main() {
	for _, arg := range os.Args[1:] {
		parts := strings.SplitN(arg, "=", 2)
		fs := DirFS(parts[1])
		http.Handle(fmt.Sprintf("/%s/git-upload-pack", parts[0]), GitUploadPackHandler{
			FS: fs,
		})
		http.Handle(fmt.Sprintf("/%s/info/refs", parts[0]), InfoRefsHandler{
			FS: fs,
		})
	}
	if err := http.ListenAndServe(":8000", nil); err != nil {
		panic(err)
	}
}

func typeSize(t, l int) []byte {
	var ret []byte
	last := byte(t<<4 | l&0xf)
	l >>= 4
	for l > 0 {
		ret = append(ret, last|0x80)
		last = byte(l & 0x7f)
		l >>= 7
	}
	return append(ret, last)
}
