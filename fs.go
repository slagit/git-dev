package main

import (
	iofs "io/fs"
	"os"
	"runtime"
)

type LstatFS interface {
	iofs.FS
	Lstat(string) (iofs.FileInfo, error)
}

type ReadlinkFS interface {
	iofs.FS
	Readlink(string) (string, error)
}

type LstatReadlinkFS interface {
	LstatFS
	ReadlinkFS
}

func containsAny(s, chars string) bool {
	for i := 0; i < len(s); i++ {
		for j := 0; j < len(chars); j++ {
			if s[i] == chars[j] {
				return true
			}
		}
	}
	return false
}

type DirFS string

func (dir DirFS) Lstat(name string) (iofs.FileInfo, error) {
	if !iofs.ValidPath(name) || runtime.GOOS == "windows" && containsAny(name, `\:`) {
		return nil, &iofs.PathError{Op: "open", Path: name, Err: iofs.ErrInvalid}
	}
	f, err := os.Lstat(string(dir) + "/" + name)
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (dir DirFS) Open(name string) (iofs.File, error) {
	if !iofs.ValidPath(name) || runtime.GOOS == "windows" && containsAny(name, `\:`) {
		return nil, &iofs.PathError{Op: "open", Path: name, Err: iofs.ErrInvalid}
	}
	f, err := os.Open(string(dir) + "/" + name)
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (dir DirFS) Readlink(name string) (string, error) {
	if !iofs.ValidPath(name) || runtime.GOOS == "windows" && containsAny(name, `\:`) {
		return "", &iofs.PathError{Op: "open", Path: name, Err: iofs.ErrInvalid}
	}
	f, err := os.Readlink(string(dir) + "/" + name)
	if err != nil {
		return "", err
	}
	return f, nil
}
